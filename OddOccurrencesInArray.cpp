#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A);
int main()
{
    vector<int> A = {9,3,9,3,9,7,9};

    int odd = solution(A);

    cout << "Input: ";
    vector<int>::iterator it = A.begin();
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;
    cout << "odd: " << odd << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 75%         | 88%
///////////////////////////////////////////

#include <map>

int solution(vector<int> &A) {

    map<int, int> m;
    vector<int>::iterator it1 = A.begin();
    for (; it1 != A.end(); it1++) {
        if(m.find(*it1) == m.end()){
            m.insert(pair <int, int> (*it1, 1));
        }else {
            m.find(*it1)->second++;
        }
    }

    int odd = -1;
    map<int, int>::iterator it2 = m.begin();
    for (; it2 != m.end(); it2++) {
        //cout << "first: " << it2->first << ", second: " << it2->second << endl;
        if(it2->second%2 == 1)
            odd = it2->first;
    }

    //cout << "odd: " << odd << endl;

    return odd;
}
