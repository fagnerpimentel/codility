#include <iostream>
#include <vector>
using namespace std;
vector<int> solution(int N, vector<int> &A);
int main()
{

    int X = 5;
    vector<int> A = {3,4,4,6,1,4,4};
    vector<int>::iterator it;

    cout << "X: " << X << endl;
    cout << "A: ";
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    vector<int> result = solution(X,A);

    cout << "Result: ";
    for (it = result.begin(); it != result.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 60%         | 77%
///////////////////////////////////////////

void max_counter(int big, vector<int> &result){
    for (unsigned int i = 0; i < result.size(); i++) {
        result.at(i) = big;
    }
}

vector<int> solution(int N, vector<int> &A) {

    int big = 0;
    vector<int> result;
    for (int i = 0; i < N; i++) {
        result.push_back(0);
    }

    for (unsigned int i = 0; i < A.size(); i++) {
        if(A.at(i) > N) max_counter(big, result);
        else {
            int ind = A.at(i)-1;
            result.at(ind)++;
            if(result.at(ind) > big)
                big = result.at(ind);
        }
    }

    //for (int j = 0; j < N; j++) {
    //    cout << result.at(j) << " ";
    //}
    //cout << endl;

    return result;
}
