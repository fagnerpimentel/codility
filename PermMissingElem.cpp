#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A);
int main()
{
    vector<int> A = {2,3,1,5};

    solution(A);

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////

#include <map>

int solution(vector<int> &A) {

    map<int, int> m;
    for (unsigned int i = 0; i < A.size()+1; i++) {
        m.insert(pair<int,int>(i+1,i+1));
    }
    vector<int>::iterator it;
    for (it = A.begin(); it != A.end(); it++) {
        m.erase(*it);
    }

    int missing = m.begin()->first;
    //cout << missing << endl;

    return missing;
}
