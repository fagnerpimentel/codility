#include <iostream>
using namespace std;
int solution(int X, int Y, int D);
int main()
{
    int X = 10;
    int Y = 85;
    int D = 30;

    cout << "X: " << X << " Y: " << Y << " D: " << D << endl;

    int count = solution(X, Y, D);

    cout << "Count: " << count << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////

int solution(int X, int Y, int D) {

    int a = Y - X;
    int b = a/D;
    if(a%D > 0)
        b += 1;

    return b;

}
