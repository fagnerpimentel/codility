#include <iostream>
using namespace std;
int solution(string &S);
int main()
{
    string s = "00:01:07,400-234-090\n";
    s += "00:05:01,701-080-080\n";
    s += "00:05:00,400-234-090\n";
    cout << s << endl;

    int bill = solution(s);

    cout << "bill: " << bill << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// %           |             | %
///////////////////////////////////////////

#include <sstream>
#include <vector>
#include <map>


//struct reg {
//  string number;
//  vector<int> time;
//  int value;
//};

int solution(string &S) {

    istringstream ss(S);
    string token;
    vector<pair<string, string>> conta;
    while(std::getline(ss, token, '\n')) {

        istringstream sss(token);
        string sub_token;
        vector<string> v;
        while(std::getline(sss, sub_token, ',')) {
            v.push_back(sub_token);
        }

        pair<string, string> registro(v.at(0),v.at(1));
        conta.push_back(registro);
    }

    vector<pair<string, vector<int> >> valores;
    for (unsigned int i = 0; i < conta.size(); i++) {

        istringstream s_time(conta.at(i).first);
        string time_value;
        vector<int> times;
        while(std::getline(s_time, time_value, ':')) {
            times.push_back(stoi(time_value));
        }


        string num = conta.at(i).second;
        num.erase(num.find('-'));

        pair<string, vector<int> > number_times(num, times);
        valores.push_back(number_times);
    }

    map<string, pair<int, int>> m;
    for (unsigned int i = 0; i < valores.size(); i++) {

        int valor = 0;
        int time_sec = valores.at(i).second.at(0)*60*60 + valores.at(i).second.at(1)*60 + valores.at(i).second.at(2);
        int time_min = valores.at(i).second.at(0)*60 + valores.at(i).second.at(1) + (valores.at(i).second.at(2)>0?1:0);
        if(time_sec <= 5*60) valor = time_sec * 3;
        else valor = time_min * 150 ;

        if(m.find(valores.at(i).first) == m.end())
            m.insert(pair<string,pair<int, int>>(valores.at(i).first, pair<int, int>(time_sec, valor)));
        else{
            m.at(valores.at(i).first).first += time_sec;
            m.at(valores.at(i).first).second += valor;
        }

    }

    string id = "";
    int big = 0;
    map<string, pair<int, int>>::iterator it;
    for (it = m.begin(); it != m.end(); it++) {
//        cout << it->first << " " << it->second.first << " " << it->second.second << endl;
        if(it->second.first >= big){
            if(it->second.first == big){
                if(id=="") continue;
                if(stoi(id) < stoi(it->first)) continue;
            }

            big = it->second.first;
            id = it->first;
        }
    }

    int bill = 0;
//    cout << id << endl;
    for (it = m.begin(); it != m.end(); it++) {
//        cout << it->first << endl;
        if(it->first == id) continue;
        bill += it->second.second;

    }




    return bill;
}
