#include <iostream>
using namespace std;
int solution(int A, int B, int K);
int main()
{

    int A = 6;
    int B = 11;
    int K = 2;

    cout << "Input:";
    cout << " A = " << A;
    cout << " B = " << B;
    cout << " K = " << K;
    cout << endl;

    int cont = solution(A,B,K);

    cout << "Numbers divisible by " << K
         << ": " << cont << endl;
    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////
/// Detected time complexity:
/// O(1)
///////////////////////////////////////////
/// CountDiv
/// Compute number of integers divisible by
/// k in range [a..b].
/// ///////////////////////////////////////


#include <vector>

int solution(int A, int B, int K) {

    int modA = A%K;
    int maxdivA = A-modA;
    int contA = maxdivA/K;

    int modB = B%K;
    int maxdivB = B-modB;
    int contB = maxdivB/K;

    int cont = contB - contA;

    if(modA == 0) cont += 1 ;
    return cont;
}
