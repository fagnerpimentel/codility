#include <iostream>
using namespace std;
int solution(int n);
int main()
{
    int r = solution(955);
    cout << r << endl;
    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// %           |             | %
///////////////////////////////////////////

int solution(int n) {
    int d[30];
    int l = 0;
    while (n > 0) {
        d[l] = n % 2;
        n /= 2;
        l++;
    }
    for (int p = 1; p < 1 + l; ++p) {
        bool ok = true;
        for (int i = 0; i < l - p; ++i) {
            if (d[l-i-1] != d[l-(i + p)-1]) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return p;
        }
    }
    return -1;
}
