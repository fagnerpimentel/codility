#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A);
int main()
{
    vector<int> A = {4,1,3,2};

    vector<int>::iterator it;
    cout << "Input: ";
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    int permutation = solution(A);

    cout << "Permutation: ";
    cout << permutation << endl;
    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////
/// Detected time complexity:
/// O(N) or O(N * log(N))
///////////////////////////////////////////
/// PermCheck
/// Check whether array A is a permutation.
/// ///////////////////////////////////////

#include <map>

int solution(vector<int> &A) {

    map<int, int> m;
    unsigned int max = 0;
    for (unsigned int i = 0; i < A.size(); i++) {
        m.insert(pair<int,int>(A.at(i),A.at(i)));
        if(A.at(i) > max) max = A.at(i);
    }

    if(A.size() < max) return 0;
    if(m.size() < A.size()) return 0;
    return 1;
}
