#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A);
int main()
{

    vector<int> A = {3,1,2,4,3};

    vector<int>::iterator it;
    cout << "Input: ";
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    int minDif = solution(A);

    cout << "Minimal differece: ";
    cout << minDif << endl;
    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////

int solution(vector<int> &A) {

    vector<int>::iterator it;

    int soma = 0;
    for (it = A.begin(); it != A.end(); it++) {
        soma += *it;
    }

    int minDif = -1;
    int d1 = 0;
    int d2 = soma;
    for (it = A.begin(); it != A.end()-1; it++) {
        d1 += *it;
        d2 -= *it;
        int dif = abs(d1 - d2);
        if(minDif == -1 || dif < minDif){
            minDif = dif;
        }
    }

    return minDif;
}
