#include <iostream>
#include <vector>
using namespace std;
int solution(vector< vector<int> > &A);
int main()
{

    vector<vector<int> > A = {
        {5,4,4},
        {4,3,4},
        {3,2,4},
        {2,2,2},
        {3,3,4},
        {1,4,4},
        {4,1,1}
    };

    for (unsigned int linha = 0; linha < A.size(); linha++) {
        for (unsigned int coluna = 0; coluna < A.at(linha).size(); coluna++) {
            cout << A.at(linha).at(coluna) << " ";
        }
        cout << endl;
    }


    int r = solution(A);
    cout << "r: " << r << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// %           |             | %
///////////////////////////////////////////

void grow(int linha, int coluna, vector< vector<int> > &A, vector< vector<bool> > &check){

    if(coluna-1 >= 0 &&
       A.at(linha).at(coluna) == A.at(linha).at(coluna-1) &&
       check.at(linha).at(coluna-1) == true) {
        check.at(linha).at(coluna-1) = false;
        grow (linha, coluna-1, A, check);
    }

    if(coluna+1 < A.at(linha).size() &&
       A.at(linha).at(coluna) == A.at(linha).at(coluna+1) &&
       check.at(linha).at(coluna+1) == true) {
        check.at(linha).at(coluna+1) = false;
        grow (linha, coluna+1, A, check);
    }

    if(linha+1 < A.size() &&
    A.at(linha).at(coluna) == A.at(linha+1).at(coluna) &&
    check.at(linha+1).at(coluna) == true) {
        check.at(linha+1).at(coluna) = false;
        grow (linha+1, coluna, A, check);
    }
}

int solution(vector< vector<int> > &A) {

    vector< vector<bool> > check;
    for (unsigned int linha = 0; linha < A.size(); linha++) {
        vector<bool> v;
        for (unsigned int coluna = 0; coluna < A.at(linha).size(); coluna++) {
            v.push_back(true);
        }
        check.push_back(v);
    }

    int cont = 0;
    for (unsigned int linha = 0; linha < A.size(); linha++) {
        for (unsigned int coluna = 0; coluna < A.at(linha).size(); coluna++) {

            if(check.at(linha).at(coluna) == false) continue;
            else {
                check.at(linha).at(coluna) = false;
                grow(linha, coluna, A, check);
                cont++;
            }
        }

    }


   return cont;
}
