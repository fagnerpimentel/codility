#include <iostream>
#include <vector>
using namespace std;
int solution(int X, vector<int> &A);
int main()
{

    int X = 5;
    vector<int> A = {1,3,1,4,2,3,5,4};

    vector<int>::iterator it;
    cout << "Input: ";
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    int time = solution(X,A);

    cout << "Time: ";
    cout << time << endl;
    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////
/// Detected time complexity:
/// O(N)
///////////////////////////////////////////
/// FrogRiverOne
/// Find the earliest time when a frog can
/// jump to the other side of a river.
///////////////////////////////////////////

#include <map>

int solution(int X, vector<int> &A) {

    map<int,int> m;
    for (int i = 1; i <= X; i++) {
        m.insert(pair<int,int>(i,0));
    }

    vector<int>::iterator it;
    for (unsigned int i = 0; i < A.size(); i++) {
        m.erase(A.at(i));
        //cout << m.size() << endl;
        if(m.size() == 0) return i;
    }

    return -1;
}
