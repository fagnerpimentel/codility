#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A);
int main()
{
    vector<int> A = {-1,-3,1};

    cout << "Input: ";
    vector<int>::iterator it = A.begin();
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    int spi = solution(A);

    cout << "Smallest positive integer: " << spi << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | 100%        | 100%
///////////////////////////////////////////
/// Detected time complexity:
/// O(N) or O(N * log(N))
///////////////////////////////////////////
/// MissingInteger:
/// Find the smallest positive integer that
/// does not occur in a given sequence.
///////////////////////////////////////////

#include <map>

int solution(vector<int> &A){

    map<int, int> m;
    for (unsigned int i = 0; i < A.size()+1; i++) {
        m.insert(pair<int,int>(i+1,i+1));
    }
    vector<int>::iterator it;
    for (it = A.begin(); it != A.end(); it++) {
        m.erase(*it);
    }

    int spi = m.begin()->first;

    return spi;
}

