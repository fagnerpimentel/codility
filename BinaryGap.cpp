#include <iostream>
using namespace std;
int solution(int N);
int main()
{
    int N = 1041;
    int gap = solution(1041);

    cout << "Number: " << N << endl;
    cout << "Gap: " << gap << endl;

    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | -           | 100%
///////////////////////////////////////////

#include <vector>

vector<int> bin;

void decimal_to_binary(int N){
    if(N == 0){
        return;
    }else{
        bin.insert(bin.begin(), N%2);
        decimal_to_binary(N/2);
    }
}

int solution(int N) {
    decimal_to_binary(N);

    int count = 0;
    int gap = 0;
    vector<int>::iterator it = bin.begin();
    for(;it != bin.end(); it++){
        if(*it == 0){
            count++;
        }
        if(*it == 1){
            if(gap < count){
                gap = count;
            }
            count = 0;
        }
    }

    return gap;
}
