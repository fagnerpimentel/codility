#include <iostream>
using namespace std;
int solution(int N);
int main()
{

    int N = 1500;
    cout << "N: " << N << endl;
    int r = solution(N);
    cout << "r: " << r << endl;
    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// %           |             | %
///////////////////////////////////////////

#include <vector>
#include <map>

int fatorial(int N){
    int fat = 1;
    for (int i = 0; i < N; i++) {
        fat *= N-i;
    }
    //cout << "fat: " << fat << endl;
    return fat;
}

int solution(int N){

    int size = 0;
    map<int, int> m;
    while (N > 0)
    {
        size++;
        int digit = N%10;
        N /= 10;

        if(m.find(digit) == m.end()) m.insert(pair<int,int>(digit,1));
        else m.find(digit)->second++;
    }

    int result = fatorial(size);
    map<int, int>::iterator it;
    for (it = m.begin(); it != m.end(); it++) {
//        cout << it->first << " " << it->second << endl;
        result /= fatorial(it->second);

        if(it->first == 0) result -= it->second;
    }

    return result;
}

