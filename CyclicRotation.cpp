#include <iostream>
#include <vector>
using namespace std;
vector<int> solution(vector<int> &A, int K);
int main()
{
    vector<int> A = {3,8,9,7,6};
    int K = 3;

    vector<int>::iterator it;
    cout << "Input: ";
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;

    cout << "Rotation: " << K << endl;

    solution(A, K);

    cout << "Output: ";
    for (it = A.begin(); it != A.end(); it++) {
        cout << *it << " ";
    }
    cout << endl;


    return 0;
}

///////////////////////////////////////////
/// Correctness | Performance | Task score
/// 100%        | -           | 100%
///////////////////////////////////////////

vector<int> solution(vector<int> &A, int K) {
    if(A.empty()) return A;

    for (int i = 0; i < K; i++) {
        A.insert(A.begin(), *(A.end()-1));
        A.pop_back();
    }

    return A;
}
